﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    abstract class Figura
    {
        protected string nombre;
        public abstract float CalcularArea();
        public void PonerNombre(string nombre)
        {
            this.nombre = nombre;
        }

        public string GetData()
        {
            return $"{nombre} : {CalcularArea()}";
        }
    }
}
