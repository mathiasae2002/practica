﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Cuadrado : Figura
    {
        private float lado;
        public Cuadrado(float lado)
        {
            this.lado = lado;
        }
        public override float CalcularArea()
        {
            return lado*lado;
        }
    }
}
