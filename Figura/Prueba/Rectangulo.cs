﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Rectangulo : Figura
    {
        private float b;
        private float altura;
        public Rectangulo(float b,float altura)
        {
            this.b=b;
            this.altura = altura;
        }
        public override float CalcularArea()
        {
            return b * altura;
        }
    }
}
