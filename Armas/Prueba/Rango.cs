﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Rango : Enemigo
    {
        private float vida;
        private float dano;
        public Rango(float vida, float dano)
        {
            this.vida = vida;
            this.dano = dano;
        }
        public override float CalcularDano()
        {
            return vida - dano;
        }
        public override float EstadoEnemy()
        {

            return vida;
        }
    }
}
