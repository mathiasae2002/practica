﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    internal class Juego
    {
        public void Ejecutar()
        {
            Menu();
        }
        private void Menu()
        {
            string vp;
            float vpi;
            string dp;
            float dpi;
            float veM = 20;
            float deM = 70;
            float veR = 90;
            float deR = 55;
            float bR = 2;
            bool secuencia = true;
            string operar = "Operar1";
            string ataque;
            Console.WriteLine("Bienvenido a mi juego. Cree a tu personaje");
            Console.WriteLine("Seleccione su vida:(No puede ser mayor a 100): ");
            vp = Console.ReadLine();
            vpi = Int16.Parse(vp);
            while (vpi > 100)
            {
                Console.WriteLine("No es valido, intentelo de nuevo");
                Console.WriteLine("Seleccione su vida:(No puede ser mayor a 100): ");
                vp = Console.ReadLine();
                vpi = Int16.Parse(vp);
            }
            Console.WriteLine("Seleccione tu dano:(No puede ser mayor a 100): ");
            dp = Console.ReadLine();
            dpi = Int16.Parse(dp);
            while (dpi > 100)
            {
                Console.WriteLine("No es valido, intentelo de nuevo");
                Console.WriteLine("Seleccione tu dano:(No puede ser mayor a 100): ");
                dp = Console.ReadLine();
                dpi = Int16.Parse(dp);
            }
            Console.WriteLine("Preparate para el combate. Melee: Vida=20. Dano=70. Rango : Vida 90. Dano 55. 2 Balas");
            while (secuencia == true) { 
            while (operar == "Operar1")
            {
                operar = "Nulo";
                Console.WriteLine("Es tu turno");
                Console.WriteLine("¿A que enemigo atacas?(Melee/Rango)");
                ataque = Console.ReadLine();
                if (ataque == "Melee" & veM > 0)
                {
                    Melee mel = new Melee(veM, dpi);
                    mel.PonerNombre("Dano Calculado");
                    Console.WriteLine(mel.CalcularDano());
                    Console.WriteLine(mel.GetDataDano());
                    veM = veM - dpi;
                    if (veM <= 0)
                    {
                        Console.WriteLine("Enemigo derrotado");
                    }

                }
                else if (ataque == "Rango" & veR > 0)
                {
                    Rango ran = new Rango(veR, dpi);
                    ran.PonerNombre("Dano Calculado");
                    Console.WriteLine(ran.CalcularDano());
                    Console.WriteLine(ran.GetDataDano());
                    veR = veR - dpi;
                    if (veR <= 0)
                    {
                        Console.WriteLine("Enemigo derrotado");
                    }

                }
                if (veM <= 0 && veR <= 0)
                {
                    Console.WriteLine("Fin del Juego. Ganaste");
                        secuencia = false;
                }
                else
                {
                    operar = "Operar2";
                }
            }
                while (operar == "Operar2")
                {
                    operar = "Nulo";
                    Console.WriteLine("Turno enemigo");
                    Melee meli = new Melee(veM, dpi);
                    Rango rangi = new Rango(veR, dpi);
                    if (rangi.EstadoEnemy() <= 0)
                    {
                        Console.WriteLine("Ataque del enemigo Melee");
                        Console.WriteLine("Dano Recibido: " + deM);
                        vpi = vpi - deM;
                        Console.WriteLine("Vida restante: " + vpi);
                    }
                    else if (meli.EstadoEnemy() <= 0)
                    {
                        Console.WriteLine("Ataque del enemigo Rango");
                        if (bR == 0)
                        {
                            Console.WriteLine("Sin balas. Pasa turno");
                        }
                        else
                        {
                            bR = bR - 1;
                            Console.WriteLine("Dano Recibido: " + deR);
                            vpi = vpi - deR;
                            Console.WriteLine("Vida restante: " + vpi);

                        }
                    }
                    else
                    {
                        Console.WriteLine("Ataque del enemigo Melee");
                        Console.WriteLine("Dano Recibido: " + (vpi - deM));
                        vpi = vpi - deM;
                        Console.WriteLine("Vida restante: " + vpi);
                        Console.WriteLine("Ataque del enemigo Rango");
                        if (bR == 0)
                        {
                            Console.WriteLine("Sin balas. Pasa turno");
                        }
                        else
                        {
                            bR = bR - 1;
                            Console.WriteLine("");
                            Console.WriteLine("Dano Recibido: " + (vpi - deR));
                            vpi = vpi - deR;
                            Console.WriteLine("Vida restante: " + vpi);

                        }
                    }
                    if (vpi <= 0)
                    {
                        Console.WriteLine("Fin del juego. Perdiste");
                        secuencia = false;

                    }
                    else
                    {
                        operar = "Operar1";
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
