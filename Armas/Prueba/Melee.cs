﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Melee : Enemigo
    {
        private float vida;
        private float dano;
        public Melee(float vida, float dano)
        {
            this.vida = vida;
            this.dano = dano;
        }
        public override float CalcularDano()
        {
            return vida - dano;
        }
        public override float EstadoEnemy()
        {

            return vida;
        }
    }
}
