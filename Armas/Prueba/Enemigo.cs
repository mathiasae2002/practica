﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    abstract class Enemigo
    {
        protected string nombre;
        public abstract float CalcularDano();
        public abstract float EstadoEnemy();
        public void PonerNombre(string nombre)
        {
            this.nombre = nombre;
        }
        public string GetDataDano()
        {
            return $"{nombre} : {CalcularDano()}";
        }
        public string GetDataEnemy()
        {
            return $"{nombre} : {EstadoEnemy()}";
        }
    }
}
